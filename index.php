
<?php
    // index.php
    Echo "OUTPUT AKHIR <br><br>";
    require_once("animal.php");  
    require_once("frog.php"); 
    require_once("ape.php"); 
    $sheep = new Animal("shaun");
    echo "Name       : ".$sheep->name."<br>"; // "shaun"
    echo "Legs       : ".$sheep->legs."<br>"; // 4
    echo "Cold Blood : ".$sheep->cold_blood."<br><br>"; // "no"  

    $kodok = new frog("Buduk");
    echo "Name       : ".$kodok->name."<br>"; // "shaun"
    echo "Legs       : ".$kodok->legs."<br>"; // 4
    echo "Cold Blood : ".$kodok->cold_blood."<br>"; // "no"  
    $kodok->jump() ; // "hop hop"
    
    $sungokong = new ape("kera sakti");
    echo "Name       : ".$sungokong->name."<br>"; // "shaun"
    echo "Legs       : ".$sungokong->legs."<br>"; // 4
    echo "Cold Blood : ".$sungokong->cold_blood."<br>"; // "no"  
    $sungokong->yell() // "Auooo"
?>
